﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Greetings7d.Core
{
    public class AppStart : IMvxAppStart
    {
        private readonly IMvxNavigationService navigationService;
        private readonly IRoomDataService roomDataService;
        
        public AppStart(IRoomDataService roomDataService, IMvxNavigationService navigationService)
        {
            this.roomDataService = roomDataService;
            this.navigationService = navigationService;
        }

        public void Start(object hint = null)
        {
            try
            {
                navigationService.Navigate(roomDataService.GetViewModelForRoomNo(0)).GetAwaiter().GetResult();
            }
            catch (System.Exception e)
            {
            }
        }
    }
}
