﻿using Plugin.SimpleAudioPlayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Greetings7d.Core.Services
{
    public class SoundPlayerService : ISoundPlayerService
    {
        readonly ISimpleAudioPlayer thunderPlayer;
        readonly ISimpleAudioPlayer footstepsPlayer;
        readonly ISimpleAudioPlayer clickPlayer;

        public SoundPlayerService()
        {
            var assembly = typeof(SoundPlayerService).GetTypeInfo().Assembly;

            Stream thunderStream = assembly.GetManifestResourceStream("Greetings7d.Core.thunder.mp3");
            thunderPlayer = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            thunderPlayer.Load(thunderStream);

            Stream footstepsStream = assembly.GetManifestResourceStream("Greetings7d.Core.footsteps.mp3");
            footstepsPlayer = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            footstepsPlayer.Load(footstepsStream);

            Stream clickStream = assembly.GetManifestResourceStream("Greetings7d.Core.click.mp3");
            clickPlayer = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            clickPlayer.Load(clickStream);
        }

        public void PlayFootsteps()
        {
            footstepsPlayer.Play();
        }

        public void PlayThunder()
        {
            thunderPlayer.Play();
        }

        public void PlayClick()
        {
            clickPlayer.Play();
        }
    }
}
