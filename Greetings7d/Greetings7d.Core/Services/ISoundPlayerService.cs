﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Greetings7d.Core.Services
{
    public interface ISoundPlayerService
    {
        void PlayFootsteps();
        void PlayThunder();
        void PlayClick();
    }
}
