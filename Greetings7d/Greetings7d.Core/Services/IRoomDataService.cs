﻿using Greetings7d.Core.Models;
using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Greetings7d.Core.Services
{
    public interface IRoomDataService
    {
        Room GetRoomForRoomNo(int roomNo);
        int Completed { get; }
        int UnCompleted { get; }
        IEnumerable<bool> RoomCompletionInfo { get; }

        IMvxViewModel GetViewModelForRoomNo(int roomNo);
        void SwitchRooms();
    }
}
