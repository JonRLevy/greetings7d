﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Greetings7d.Core.ViewModels
{
    public class NorthViewModel : G7dBaseViewModel
    {
        public NorthViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(1, roomDataService, soundPlayerService, navigationService)
        {
        }
    }
}
