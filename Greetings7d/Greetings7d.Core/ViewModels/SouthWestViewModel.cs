﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;

namespace Greetings7d.Core.ViewModels
{
    public class SouthWestViewModel : G7dBaseViewModel
    {
        public SouthWestViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(6, roomDataService, soundPlayerService, navigationService)
        {
        }
    }
}
