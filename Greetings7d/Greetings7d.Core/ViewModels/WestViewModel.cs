﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Greetings7d.Core.ViewModels
{
    public class WestViewModel : G7dBaseViewModel
    {
        public WestViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(3, roomDataService, soundPlayerService, navigationService)
        {
        }
    }
}
