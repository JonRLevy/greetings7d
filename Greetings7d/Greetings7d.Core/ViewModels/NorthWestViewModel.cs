﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Greetings7d.Core.ViewModels
{
    public class NorthWestViewModel : G7dBaseViewModel
    {
        public NorthWestViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(0, roomDataService, soundPlayerService, navigationService)
        {
            annoyanceFactor = 90.0;
        }
    }
}
