﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;

namespace Greetings7d.Core.ViewModels
{
    public class SouthViewModel : G7dBaseViewModel
    {
        public SouthViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(7, roomDataService, soundPlayerService, navigationService)
        {
        }
    }
}
