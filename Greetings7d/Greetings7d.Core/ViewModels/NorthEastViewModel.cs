﻿using Greetings7d.Core.Services;
using MvvmCross.Core.Navigation;
using System.Text;

namespace Greetings7d.Core.ViewModels
{
    public class NorthEastViewModel : G7dBaseViewModel
    {
        public NorthEastViewModel(IRoomDataService roomDataService, ISoundPlayerService soundPlayerService, IMvxNavigationService navigationService)
            : base(2, roomDataService, soundPlayerService, navigationService)
        {
        }

        protected override void OnAnnoyanceFactorChanged()
        {
            RaisePropertyChanged(nameof(RoomContentText));
        }

        public override string RoomContentText
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (char c in thisRoom.ContentText)
                {
                    sb.Append((char)(c ^ ((short)AnnoyanceFactor * (short)AnnoyanceFactor)));
                }

                return sb.ToString();
            }
        }
    }
}
