﻿using Greetings7d.Core.Services;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace Greetings7d.Core
{
    public class CoreApp : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterCustomAppStart<AppStart>();
        }
    }
}
